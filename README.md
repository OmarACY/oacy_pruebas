[{]: <region> (header)
# Pruebas
[}]: #
[{]: <region> (body)

### Objetivo del proyecto
Tener ejemplos sobre componentes de Android Studio para reciclar codigo en otras aplicaciones.
### Como empezar...

1. Clonar repositorio (Utilizar URL designada para cada miembro del equipo)

       $ git clone https://"my-username"@bitbucket.org/OmarACY/oacy_pruebas.git
	   
2. Iniciar Android Studio y abrirlo.

3. Esperar a que se instalen las dependencias del proyecto.

4. Generar APK en la pestaña Build -> Generar APK de Android Studio

Nota.- Este proyecto fue desarrollado en Android Studio.

[}]: #
[{]: <region> (footer)
[{]: <helper> (nav_step)
### 2017
### Contacto ###

Omar Alejandro Cervantes Yepez, OmarAlejandroCY@gmail.com

[}]: #
[}]: #