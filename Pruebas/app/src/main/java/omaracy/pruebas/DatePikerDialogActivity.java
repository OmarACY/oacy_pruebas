package omaracy.pruebas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Locale;

public class DatePikerDialogActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    EditText mDateView; //EditText is save the date chosen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_piker_dialog);

        mDateView = (EditText) findViewById(R.id.etODPD);
        mDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDatePiker().show();
            }
        });
    }

    /*
    * Method for create a dialogDatePiker
    * */
    private Dialog dialogDatePiker(){
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,this, year, month,day);
        datePickerDialog.setTitle(getResources().getString(R.string.date));
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        return datePickerDialog;
    }

    /*
    * Method that is executed when the date is chosen
    * */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //String myFormat = "MM/dd/yy"; //In which you need put here
        //SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        //TODO:here take the date chose for the user
        mDateView.setText(String.format(Locale.getDefault(),"%d/%d/%d ",dayOfMonth,month+1,year));
    }
}
