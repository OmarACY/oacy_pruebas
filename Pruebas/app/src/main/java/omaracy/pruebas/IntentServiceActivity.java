package omaracy.pruebas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class IntentServiceActivity extends AppCompatActivity {
    private EditText mEntrance;
    public static TextView mExit; //This method is not recommended for modify interface

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_service);

        mEntrance = (EditText) findViewById(R.id.entrance);
        mExit = (TextView) findViewById(R.id.exit);
    }

    public void calculateOperation(View view){
        double n = Double.parseDouble(mEntrance.getText().toString());
        mExit.append(n + "^2 = ");
        Intent i = new Intent(this,IntentServiceOperation.class);
        i.putExtra("numero",n);
        startService(i);
    }
}
