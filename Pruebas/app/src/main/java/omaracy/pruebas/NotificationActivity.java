package omaracy.pruebas;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore.Audio;
import android.renderscript.RenderScript;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class NotificationActivity extends AppCompatActivity {

    private static final int ID_NOTIFICATION_CREATE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        findViewById(R.id.btnNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNotification();
            }
        });
    }

    /*
    * Method for launch notification
    * */
    private void launchNotification(){

        /*
        *
        * */
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle("¡SOCORRO!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("S.O.S.")
                .setSound(Uri.parse("android.resource://"+ getPackageName() +"/"+R.raw.notificacion))
                .setVibrate(new long[]{0, 100, 100, 100, 100, 100,100, 300, 100, 300, 100, 300,100, 100, 100, 100, 100, 100})
                .setLights(Color.RED,3000,1000)
                .setTicker("¡SOCORRO!")
                .setAutoCancel(true);


        //Add a intent to notification
        PendingIntent pendingIntent= PendingIntent.getActivity(this,0,new Intent(this,NotificationActivity.class),0);//For the notification, open the activiti to touch
        notification.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_NOTIFICATION_CREATE,notification.build());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            this.finish();
            startActivity(new Intent(this,MainActivity.class));
            //this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
