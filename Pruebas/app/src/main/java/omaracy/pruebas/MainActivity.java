package omaracy.pruebas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnDatePikerDialog).setOnClickListener(this);
        findViewById(R.id.btnTimePikerDialog).setOnClickListener(this);
        findViewById(R.id.btnServices).setOnClickListener(this);
        findViewById(R.id.btnIntentService).setOnClickListener(this);
        findViewById(R.id.btnNotification).setOnClickListener(this);
        findViewById(R.id.btnCustomListView).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = null;
        switch (v.getId()){
            case R.id.btnDatePikerDialog:
                i = new Intent(this,DatePikerDialogActivity.class);
                break;
            case R.id.btnTimePikerDialog:
                i = new Intent(this,TimePikerDialogActivity.class);
                break;
            case R.id.btnServices:
                i = new Intent(this,ServiceActivity.class);
                break;
            case R.id.btnIntentService:
                i = new Intent(this,IntentServiceActivity.class);
                break;
            case R.id.btnNotification:
                i = new Intent(this,NotificationActivity.class);
                break;
            case R.id.btnCustomListView:
                i = new Intent(this,CustomListViewActivity.class);
                break;
        }

        if(i != null) {
            startActivity(i);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            this.onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
