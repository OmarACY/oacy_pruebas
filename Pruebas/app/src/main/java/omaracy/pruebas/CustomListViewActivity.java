package omaracy.pruebas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import omaracy.pruebas.Adapters.MyAdapter;
import omaracy.pruebas.Models.ItemPhoto;

public class CustomListViewActivity extends AppCompatActivity {

    private ListView mPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);

        mPhotos = (ListView) findViewById(R.id.lvPhotos);
        generateList();

        mPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),String.valueOf(i),Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    * Method for the Generate ListView with Adapter View
    * */
    private void generateList(){
        LinkedHashMap<String, ItemPhoto> renglon = new LinkedHashMap<>();

        renglon.put("0", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));
        renglon.put("1", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));
        renglon.put("2", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));
        renglon.put("3", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));
        renglon.put("4", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));
        renglon.put("5", new ItemPhoto("auto2.jpg","JPG","1.5 MB"));


        List<ItemPhoto> item =new ArrayList<>(renglon.values());
        MyAdapter adapter= new MyAdapter(this, item);
        mPhotos.setAdapter(adapter);
    }

}
