package omaracy.pruebas;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimePikerDialogActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {

    EditText mTimeView; //EditText is save the time chosen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_piker_dialog);

        mTimeView = (EditText) findViewById(R.id.etTime);
        mTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTimePiker().show();
            }
        });
    }

    /*
    * Method for create a dialogTimePiker
    * */
    private Dialog dialogTimePiker(){
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,this/*Implementarcion el metodo onTimeSet*/, hour, minute,false);

        timePickerDialog.setTitle(getResources().getString(R.string.time));
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //TODO:here take the date chose for the user
        mTimeView.setText(String.format(Locale.getDefault(),"%2d:%2d ",hourOfDay,minute));
    }
}
