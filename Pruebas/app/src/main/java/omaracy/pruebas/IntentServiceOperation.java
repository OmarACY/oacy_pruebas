package omaracy.pruebas;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

/**
 * Created by omaracy on 28/06/17 project Pruebas.
 */

public class IntentServiceOperation extends IntentService {

    public IntentServiceOperation(){
        super("IntentServiceOperation");
    }

    @Override
    protected void onHandleIntent(Intent i) {
        double n = i.getExtras().getDouble("numero");
        SystemClock.sleep(5000);
        //IntentServiceActivity.mExit.append(n*n + "\n"); //This method is not recommended for modify interface
    }
}
