package omaracy.pruebas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class ServiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        findViewById(R.id.btnStart).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startService(new Intent(ServiceActivity.this, ServiceMusic.class));
            }
        });

        findViewById(R.id.btnStop).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                stopService(new Intent(ServiceActivity.this, ServiceMusic.class));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // Esto es lo que hace mi botón al pulsar ir a atrás
            this.finish();
            startActivity(new Intent(this,MainActivity.class));
            //this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
