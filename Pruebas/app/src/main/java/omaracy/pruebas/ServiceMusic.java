package omaracy.pruebas;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Created by omaracy on 29/06/17.
 */

public class ServiceMusic extends Service {

    private static final int ID_NOTIFICATION_CREATE = 1;

    MediaPlayer player;

    @Override
    public void onCreate() {
        Toast.makeText(this,"Servicio creado",
                Toast.LENGTH_SHORT).show();
        player = MediaPlayer.create(this, R.raw.sweet_dreams);
    }

    /*
    * START_STICKY .- If we want the system to try to create the service again when there is enough memory, intenc equals null
    * START_NOT_STICKY .- If we want the service to be created again when a new creation request arrives
    * START_REDELIVER_INTENT .- The system will try to re-create the service, intenc equals previous
    * START_STICKY_COMPATIBILITY .- Compatible version of START_STICKY that does not guarantee that onStartCommand() will be called after the process is killed
    * */
    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle("Creando Servicio de musica")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("Informacion adicional");

        //Add a intent to notification
        PendingIntent pendingIntent= PendingIntent.getActivity(this,0,new Intent(this,ServiceActivity.class),0);//For the notification, open the activiti to touch
        notification.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(ID_NOTIFICATION_CREATE,notification.build());

        Toast.makeText(this,"Servicio arrancado "+ idArranque,
                Toast.LENGTH_SHORT).show();
        player.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(ID_NOTIFICATION_CREATE);
        Toast.makeText(this,"Servicio detenido",
                Toast.LENGTH_SHORT).show();
        player.stop();
    }

    @Override
    public IBinder onBind(Intent intencion) {
        return null;
    }
}
