package omaracy.pruebas.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import omaracy.pruebas.Models.ItemPhoto;
import omaracy.pruebas.R;

/**
 * Created by omaracy on 08/07/17 project Pruebas.
 */

public class MyAdapter extends ArrayAdapter<ItemPhoto> {

    public MyAdapter(Context context, List<ItemPhoto> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Obtener inflater.
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // ¿Existe el view actual?
        if (null == convertView) {
            convertView = inflater.inflate(
                    R.layout.item_list_view,
                    parent,
                    false);
        }

        ImageView photo = (ImageView) convertView.findViewById(R.id.ivPhoto);
        TextView name = (TextView) convertView.findViewById(R.id.tvName);
        TextView type = (TextView) convertView.findViewById(R.id.tvType);
        TextView size = (TextView) convertView.findViewById(R.id.tvSize);

        ItemPhoto item = getItem(position);

        Random random = new Random();
        int imageResource;

        switch (random.nextInt(3)){
            case 0: imageResource = R.drawable.auto1; break;
            case 1: imageResource = R.drawable.auto2; break;
            case 2: imageResource = R.drawable.auto3; break;
            default:imageResource = R.drawable.auto1; break;
        }

        photo.setImageResource(imageResource);

        if (item != null) {
            name.setText(item.getName());
            type.setText(item.getType());
            size.setText(item.getSize());
        }
        ;

        return convertView;
    }

}
