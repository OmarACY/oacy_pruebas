package omaracy.pruebas.Models;

/**
 * Created by omaracy on 9/07/17.
 */


public class ItemPhoto {

    private String name;
    private String type;
    private String size;

    /*
    * Constructor model photo
    * */
    public ItemPhoto(String name, String type, String size) {

        this.name = name;
        this.type = type;
        this.size = size;
    }
    /*
    * return name photo
    * */
    public String getName(){
        return name;
    }

    /*
    * @return extencion type photo
    * */
    public String getType(){
        return type;
    }

    /*
    * @return Name photo
    * */
    public String getSize(){
        return size;
    }

    /*
    * set name photo
    * */
    public void setName(String name){
        this.name = name;
    }

    /*
    * set type photo
    * */
    public void setType(String type){
        this.type = type;
    }

    /*
    * set size photo
    * */
    public void setSize(String size){
        this.size = size;
    }
}
